with import <nixpkgs> {}; 

stdenv.mkDerivation {
  name = "puissance4";
  buildInputs = [ 
    cmake
    pkgconfig
    cpputest
    gdb
    gnome3.gtkmm
    openssl
    zlib
  ];
  src = ./.;
}

