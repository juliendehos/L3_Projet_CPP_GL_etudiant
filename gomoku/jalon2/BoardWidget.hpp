#ifndef BOARD_WIDGET_HPP
#define BOARD_WIDGET_HPP

#include <Wt/WPaintedWidget.h>
#include <Wt/WPainter.h>

class AppGomoku;

class BoardWidget : public Wt::WPaintedWidget {
    private:
        AppGomoku & _appGomoku;

    public:
        BoardWidget(AppGomoku & appGomoku);

    protected:
        void paintEvent(Wt::WPaintDevice * paintDevice);

    private:
        void handleClick(const Wt::WMouseEvent & event);
};

#endif

