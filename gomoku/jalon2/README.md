
## docs pour Wt

- https://www.webtoolkit.eu/wt/doc/reference/html/index.html

- https://www.webtoolkit.eu/widgets

- https://github.com/emweb/wt/

## pour ajouter Wt comme submodule git

```
git submodule add https://github.com/emweb/wt ext/wt
cd ext/wt
git checkout 4.0.4
```

et décommenter les lignes correspondantes dans le CMakeLists.txt

## pour lancer le programme serveur

```
./gomoku-server --docroot . --http-address 0.0.0.0 --http-port 3000
```

