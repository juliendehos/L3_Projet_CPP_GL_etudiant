#include "BoardWidget.hpp"

#include "AppGomoku.hpp"

#include <sstream>

BoardWidget::BoardWidget(AppGomoku & appGomoku) : 
    Wt::WPaintedWidget(),
    _appGomoku(appGomoku)
{
	mouseWentDown().connect(this, &BoardWidget::handleClick);
    resize(400, 300); 
}

void BoardWidget::paintEvent(Wt::WPaintDevice * paintDevice) {
    Wt::WPainter painter(paintDevice);
    Wt::WPen pen;

    painter.setBrush(Wt::WBrush(Wt::StandardColor::DarkGreen));
    pen.setColor(Wt::StandardColor::Black);
    pen.setWidth(Wt::WLength(4.0));
    painter.setPen(pen);
    painter.drawRect(0, 0, 400, 300);
}

void BoardWidget::handleClick(const Wt::WMouseEvent & event) {
    _appGomoku.count();
}

